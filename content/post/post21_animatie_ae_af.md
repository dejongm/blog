---
title: Animatie AE af
date: 2019-09-27
---

Kort dagje gehad. Ik mocht rond 12u stoppen omdat de challenge day van gisteren nogal lang was. In de ochtend heb ik de eerste versie van de animatie volledig afgemaakt. Ik heb de ‘explosie’ van de koker vloeiender gemaakt en het project met een plug-in geëxporteerd zodat de developers het kunnen gebruiken op de website.

4 uur gewerkt vandaag, 40 uur totaal.
