---
title: Emailtemplate RAAK in mailchimp + DUI1
date: 2019-09-06
---

Dag begonnen met standup meeting. Kort vertelt wat ik de vorige dag had gedaan.
Vervolgens van Fabian de taak gekregen om verder te gaan met het email template voor RAAK. Hij heeft mij gevraagd of ik deze kon omzetten naar Mailchimp. Ik had nog nooit met Mailchimp of iets soortgelijks gewerkt dus dit was een mooi leermoment.

Alle bestaande templates waren via HTML gecodeerd, maar sinds ik hier in deze context niet zoveel verstand van had, ben ik aan de slag gegaan met de drag en drop builder. Het was enigszins gelukt om het template in Mailchimp te zetten maar dit ging best wel moeizaam.

Ik heb door het testen ook geleerd tegen wat voor beperkingen je aanloopt bij het ontwerpen van een email:
-	Je mag geen belangrijke informatie in een afbeelding zetten. Niet elke mailclient (Outlook) kan afbeeldingen lezen. Je krijgt dan een blanco mail te zien.
-	Gebruik fonts dat ieder apparaat kan lezen zoals Arial of Helvetica. Anders kan je problemen krijgen met de fontweergave.

Alex is vervolgens ook naar mij toe gekomen om feedback te geven op het template. Hij heeft toen ook vertelt dat ik template het beste kan baseren op het bestaande template zodat er minder werk is voor de developers om het te realiseren. Ik ben daarom opnieuw begonnen met een nieuwe versie van het template. Deze had ik aan het einde van de dag afgerond. Ook heb ik gewerkt aan een nieuwe daily UI challenge: het maken van een sign up page.

Op beide deliverables heb ik feedback gevraagd bij Mohsen, hij heeft een paar kleine visuele tips gegeven die ik heb opgeschreven voor maandag.

8 uur gewerkt vandaag, 40 uur totaal deze week.
