---
title: Debriefing BE + uitnodiging uitje
date: 2019-10-02
---

Zoals eerder vertelt heb ik momenteel een eigen projectje om de website van mijn broer een redesign te geven. Ik heb hiervoor een plan van aanpak geschreven, dat goed is gekeurd door Rick. Vervolgens heb ik mijn broer gebeld om concreet af te spreken wat hij nou graag wilt en voor wat de website momenteel gebruikt wordt. Dit was een goed gesprek waar ik veel bruikbare informatie uit heb kunnen halen.

Vandaag had ik dit aan Rick voorgelegd. Hij vertelde dat de volgende stap in het proces is om een debriefing te maken. Hier ben ik dan ook mee aan de slag gegaan. In deze debriefing heb ik de gegevens van de opdrachtgever en nemer opgenomen en wat vertelt over het bedrijf, aanleiding opdracht, ontwerpdoel, planning en kosten. Met deze eerste versie ben ik naar Rick gegaan. Hij gaf als feedback:
-	Geen ik of hij vorm gebruiken, behandel het als een officiële debriefing van bedrijf tot bedrijf
-	Aangeven waarom BE een moderne uitstraling belangrijk vindt
-	Vertel dat de haalbaarheid iPhone uploaden nog onderzocht moet worden
-	Stukje over handleiding schrijven mag eruit, dit wordt eerder een onderdeel van een user story
-	Vaagheid van SEO-optimalisatie mag eruit, zet het concreet neer en dan kan je later alsnog kijken of je het gaat doen of niet
-	Neem in de sprints ook ontwerppresenaties op voor tussentijdse feedback
-	Verander het woord kosten naar investering. Dit klinkt positiever

Deze wijzigingen heb ik toegepast en vervolgens weer naar Rick gestuurd. Hij was er nu tevreden mee. Ik heb daarom mijn broer geïnformeerd met de vraag of hij een akkoord wil geven op de debriefing.
Tevens heeft Gitva mij benaderd met de vraag of ik een uitnodiging wilde maken voor het teamuitje volgende week. Zij heeft mij informatie geleverd zoals wie er allemaal komen, waar het is, wat we gaan doen en hoelaat het begin. Het betreft een kookworkshop dus kwam ik met het idee om de uitnodiging in een recept vorm te presenteren. Ik heb hiervoor een eerste opzet gemaakt met alleen de informatie. Dit heb ik vervolgens aan Gitva laten zien die het een leuk idee vondt.

De rest van de dag heb ik aan deze uitnodiging gewerkt. Op het einde van de dag heb ik ook feedback aan Rick gevraagd. Hij gaf de volgende feedback:

-	Tijd, wat en waar is het belangrijkst. Maak dit dan ook een groter font.
-	Misschien leuker om de decoratieve elementen over de hele breedte van de uitnodiging te doen.
-	Gedeelte over ingredienten kan kleiner zodat je meer ruimte hebt voor de tijd. Ingrediënten zijn toch alleen een knipoog.
-	Afsluiter “Bon appetit” hetzelfde font als de rest van de tekst i.p.v. hetzelfde font als de koppen.
-	Verwerk het woord uitje in de tekst omdat je ook een afbeelding hebt met een ui.

8 uur gewerkt vandaag.
