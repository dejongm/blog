---
title: Teambuilding sessie + kalender awards
date: 2019-10-03
---

In de ochtend was er een teambuilding sessie met het ontwerpteam. Dit werd verzorgd door een externe begeleider. Hier werden allerlei zaken besproken over dingen die beter kunnen etc en hoe mensen de samenwerking momenteel vinden. Dit was een interessante ervaring om bij te wonen.

In de middag heb ik mijn verbeterde versie van de uitnodiging laten zien aan Gitva. Zij was er erg tevreden mee, dus hebben wij deze uitgeprint, opgerold en gestrikt zodat deze aanstaande maandag uitgedeeld kunnen worden.s

Tevens heeft Gitva mij gevraagd of ik een kalender kon maken voor alle design awards het aankomende jaar. Ik heb een lijst met verschillende jaarlijkse awards aangeleverd gekregen. Aan mij de vraag om inlever deadlines en prijsuitreiking datums te vinden en aan deze lijst toe te voegen. Nadat ik dit af had ben ik verder gegaan met wat schoolwerkzaamheden zoals mijn blog en Miro bijwerken.

Aan het eind van de dag had ik een gesprek met Rick over mijn stagevoortgang. Was weer een prima gesprek. Rick heeft mij het idee gegeven om een PechaKucha presentatie te houden voor mijn leerdoel. Bij deze stijl verandert er om de 20 seconden de slide dus je moet snel en concreet kunnen uitleggen wat er op de slide staat. Tevens heb ik wat hulp gekregen met het maken van een wekelijkse planning voor mijzelf. Dit gaat me denk ik goed helpen met inzicht in waar ik mee bezig ben elke dag.

8 uur gewerkt vandaag.
