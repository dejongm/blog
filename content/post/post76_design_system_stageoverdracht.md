---
title: Design system af + stageoverdracht
date: 2019-12-05
---

In de ochtend had ik een afspraak bij de dokter en orthodontist. Rond de middag was ik weer op kantoor. Ik heb vandaag gewerkt aan de afronding van het design system voor Betlej Elektrotechniek. Toen ik hier klaar mee was heb ik feedback aan Fabian gevraagd. Hij gaf een paar kleine verbeterpunten:
-	Zet in je overzicht de totale breedte van de verschillende elementen
-	Voeg nog een sectie toe over tone of voice. Dit gaat niet alleen over spreektaal maar ook over hoe je ontwerpen aansluiten bij de kernwaarden van BE

Aan het eind van de middag heb ik ook een overdrachtsmoment gehad met Rick en Fabian. Rick gaat met vakantie dus nu is Fabian tijdelijk mijn stagebegeleider. In dit gesprek hebben we besproken waar ik nog aan moet werken mbt mijn leerdoelen. En wat mijn planning wordt voor volgende week.

4 uur gewerkt vandaag.
