---
title: Blog uploaden + onderzoeksvragen AH
date: 2019-11-25
---

In de ochtend bezig geweest met mijn blog, die ik in Word heb getypt, online te zetten voor het stageverslag. Dit duurde even, dus hier was ik heel de ochtend mee bezig.
In de middag heb ik een begin gemaakt aan het ethiek onderzoek voor de Albert Heijn. Daarvoor heb ik de volgende hypothese opgesteld die ik wil valideren: "Het privacybeleid van de Albert Heijn voldoet aan de richtlijnen van ethisch ontwerp."
Daarvoor heb ik de volgende onderzoeksvragen opgesteld:
-	Wat is ethisch ontwerp?
-	Hoe wordt ethisch ontwerp toegepast bij de concurrentie?
-	Hoe wordt ethisch ontwerp toegepast bij het privacybeleid van Albert Heijn?

Vervolgens heb ik een begin gemaakt aan een onderzoek door het gebruik van onderstaande websites:
-	https://www.smashingmagazine.com/2018/03/using-ethics-in-web-design/
-	https://designshack.net/articles/business-articles/what-are-design-ethics-and-why-are-they-important/
-	https://www.invisionapp.com/inside-design/what-does-ethical-design-mean/

Ook heb ik verder gewerkt aan DUI13: Direct Messaging app. Dit wil ik doen met een RuneScape thema. Zo heb ik iconen verzameld en deze uitgeknipt met PhotoShop en heb ik een navigatiebalk gemaakt.

8 uur gewerkt vandaag.
