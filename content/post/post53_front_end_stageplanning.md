---
title: Front-end overleg + stageplanning
date: 2019-11-04
---

Vandaag met Sander ingepland om aan front-end development te werken. Na projectoverleg heeft hij mij het een en ander uitgelegd over best practices, hoe ik mijn problemen met de progressbar kan oplossen en hoe bepaalde dingen werken bij het programmeren. Hier heb ik de rest van de ochtend aan besteed.

Net voor de lunch kan Rick naar mij toe met verzoek om een planning te maken voor de rest van de stageperiode. Rick gaf als tip om te beginnen bij de einddatum en vanaf daar terug te werken. Ik heb als eerste een planning gemaakt met alle taken die ik vanaf deze week t/m februari ga doen.

Vervolgens gaf Rick als feedback om alle taken per dag in te delen. Dus blokken van vijf dagen maken en daar de werkzaamheden in verdelen. Alle belangrijke deadlines kunnen dan bovenaan en aan dagen gekoppeld worden.

Toen ik dit eenmaal had, ben ik weer naar Rick gegaan. Hij vond de planning nu goed ingedeeld. Nu de opdracht om per taak uren eraan koppelen. Op deze manier heb je een handig overzicht en weet je waar je per dag aan toe bent. Vervolgens kan ik aan het begin van eerdere week concrete opdrachtjes te maken om mijn taken te voltooien. Toen ik hier eenmaal klaar mee was, was het alweer het einde van de dag.

8 uur gewerkt vandaag.
