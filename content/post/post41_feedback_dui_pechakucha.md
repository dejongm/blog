---
title: Interview tweedejaars + oefenen PechaKucha + debriefing RET
date: 2019-10-16
---

In de ochtend laatste wijzigingen aan de pechakucha presentatie verwerkt.

Rond 10u is er een tweedejaars langs geweest om mij te interviewen over mijn stage en hoe ik het solliciteren heb ervaren. Was een leuk gesprek. Ook heeft ze wat foto’s gemaakt van het kantoor en heb ik haar een rondleiding gegeven.
Bezoek tweedejaars
In de middag een briefing gehad over een nieuwe, korte, design challenge. De RET heeft een soort NS-Business Card, maar deze verkoopt niet helemaal. Aan ons de vraag om de landingspagina van hun zakelijke website opnieuw te ontwerpen om meer zakelijke klanten aan RET te koppelen.

Kort hierna was het alweer tijd om met Rick mijn presentatie te oefenen. Deze keer vond ik erg goed gaan, Rick was er ook erg tevreden mee. Ik ben er daarom zeker van dat het morgen goed moet komen.

8 uur gewerkt vandaag.
