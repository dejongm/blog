---
title: Front-end WeConnekt + DUI11 + voortgangspresentatie BE
date: 2019-10-29
---

Vandaag had ik met onze developer Sander afgesproken dat ik zou beginnen aan een stukje frontend voor de WeConnekt-app. Hierbij is het de bedoeling dat ik een progressbar volgens ontwerp namaak met een tekstuele aanduiding van de voortgang. Helaas was hij vandaag afwezig i.v.m. ziekte. Daarom heb ik met Rick afgesproken dat ik vandaag kijk hoe ver ik kom. Als ik te lang op iets vast blijf zitten kan ik beter aan iets anders gaan werken.

Ik ben daarom begonnen met onderzoeken hoe ik een progress bar kan maken. Hierdoor kwam bij het progress element en div element. Als eerste heb ik het progress element gebruikt, maar hierdoor kwam ik erachter dat ik voor iedere browsers verschillende code moet schrijven, dus daarom leek het me makkelijker om een div element te gebruiken. Toen ik eenmaal de kleur en hoogte x breedte goed had, had ik moeite om tekst in de balk te krijg. Al snel liep ik vast op het instellen van de margin/padding van de tekst. Daarom heb ik maar besloten om voor nu te stoppen en te documenteren wat ik heb gedaan en waar ik op vast loop.

Vervolgens ben ik van start gegaan met een nieuwe daily UI challenge. Hiervoor moest ik een “flash message” maken. Ik wist nog niet zo goed wat ik hiervan moest verwachten dus ben ik zoals altijd begonnen met het opzoeken van inspiratie op dribbble. Hierdoor kwam ik er al snel achter dat het neerkomt op een melding maken voor wanneer iets goed gaat en wanneer iets fout gaat. Ik heb daarom besloten om twee meldingen te ontwerpen voor een koffiebar waarbij een bestelling gelukt is en één waarbij de bestelling mislukt is. Ik heb daarom de eerste schetsen gemaakt voor een blije koffiebeker en een koffiebeker die om is gevallen waar koffie uit stroomt.

Toen ik eenmaal wat schetsen had was het alweer tijd voor de lunch. Na de lunch ben ik verder gegaan met het Betlej Elektrotechniek project. Hiervoor heb ik nog eens kritisch gekeken naar het tweede ontwerp dat ik voor het BE project heb gemaakt. Ik vond dat deze versie nog te veel lijkt op de eerste versie die ik heb gemaakt. Daarom heb ik de sectie over de geleverde diensten en recente projecten een nieuwe, eigen stijl gegeven. Tevens heb ik een mobiele versie uitgewerkt voor de eerste versie van mijn ontwerp.

Aan het eind van de middag heb ik afgesproken met Rick een voortgangspresentatie te doen over Betlej. Hier heb ik gepresenteerd wat de status is van het project en waar ik tegen aan ben gelopen. We hebben hier afgesproken dat ik met Fabian ga zitten om mijn Trello beter in te richten. Ook hebben we samen mijn ontwerpen doorgenomen, waar ik feedback voor heb verzameld.

8 uur gewerkt vandaag.
