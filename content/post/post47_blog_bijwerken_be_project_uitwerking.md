---
title: Blog bijwerken + BE project verdere uitwerking
date: 2019-10-25
---

In de ochtend bezig geweest met het opruimen van mijn laptop en mijn blog bijwerken.

In de middag verder gewerkt aan de visuele ontwerpen voor het BE project. Hiervoor heb ik de feedback van Mohsen uitgewerkt tot een nieuwe versie van mijn ontwerp. Toen ik eenmaal wat had waar ik tevreden mee was, ben ik weer naar Mohsen gegaan voor feedback. Hij gaf als feedback:
-	Probeer je secties uit te lijnen in kaders van 800px
-	Gebruik minder witruimte, dit kan opgelost worden door bovenstaande te doen
-	Knop van ‘meer projecten’ kan je ook subtiel naast de titel verwerken
-	Gebruik een andere kleur dan blauw voor de secundaire knoppen
-	Volgorde info voor contactformulier klopt niet

Vervolgens de rest van de dag bezig geweest om deze feedback te verwerken. Aan het eind van de dag had ik een volledige uitwerking af. Volgende week beginnen aan het volgende ontwerp.

8 uur gewerkt vandaag, 32 uur totaal vandaag (1 dag verlof).
