---
title: Presentatie terugkomdag + DUI2
date: 2019-09-10
---

Vandaag was een rustige dag. Op dit moment was er niet echt iets dat ik kon doen voor de huidige design challenge. Ik heb me daarom voornamelijk met werk voor mezelf beziggehouden.

Als eerste ben ik aan de slag gegaan met het maken van een korte presentatie voor de terugkomdag. In deze presentatie heb ik toegelicht wie ik ben, wat ik wil leren en bij wat voor soort bedrijf ik nu stage loop.

Vervolgens heb ik gewerkt aan een nieuwe Daily UI Challenge. Ik heb daarom een creditcard checkout gemaakt voor Nike. Hiervoor heb ik eerst wat inspiratie opgezocht en vervolgens ben ik aan de slag gegaan waardoor de rest vanzelf kwam.

De volgende opdracht was iets pittiger, een landing page naar keuze maken. Ik heb daarom besloten voor een bar/restaurant te gaan met een Zuid-Amerikaans thema. Zoals altijd weer inspiratie opgezocht, maar ik liep vast. Ik wilde mezelf uit te dagen door zelf wat illustraties te maken i.p.v. alleen maar foto’s en bestaande iconen te gebruiken. Ik liep hierdoor vast want ik wist niet zo goed hoe ik hieraan moest beginnen.

Ik heb daarom aan Fabian gevraagd hoe hij er mee zou beginnen. Hij vertelde me:
-	Kies een hoofdonderwerp, maar dat heb je al
-	Verzamel vervolgens dingen die je wilt presenteren of verkopen aan de klant. Bijv wat voor eten? Drank? Bied je iets unieks?
-	Bepaal wat voor gevoel je wilt meegegeven. Is het een premium restaurant? Of juist heel goedkoop?
-	Ga opzoek naar inspiratie voor het gene dat je hebt gekozen.
-	Maak een moodboard dat aan die criteria voldoet
-	Vervolgens kan je beginnen met schetsen maken en waarschijnlijk komt de rest dan vanzelf

Dit heeft me goed op weg geholpen. Het was al laat op de middag dus de rest van de dag ben ik bezig geweest met duidelijk te maken wat voor soort restaurant/bar het wordt en ben ik aan de slag gegaan met een moodboard.

8 uur gewerkt vandaag.
