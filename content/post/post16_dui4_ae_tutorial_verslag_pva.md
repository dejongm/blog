---
title: DUI4 + AE tutorial + Scrum verslag + PvA BE
date: 2019-09-20
---

Vandaag gewerkt aan de volgende daily ui challenge: het maken van een calculator. Deze vrij vlot afgerond, was niet zo’n hele uitdagende opdracht.

Verder heb ik heel de ochtend gewerkt aan het animeren van een plaatje in After Effects met de hulp van een tutorial.

In de middag werd door Rick aan mij gevraagd om alle copy van de ABR lander in een Word document uit te werken zodat dit naar de opdrachtgever gestuurd kan worden voor beoordeling.

Ook heb ik a.d.h.v. het gesprek met Mike een verslag geschreven over hoe Scrum in elkaar zit en hoe het bij RAAK wordt geïmplementeerd.

Gisteren heb ik tijdens het stage overleg met Rick besproken dat ik een eigen project ga organiseren a.d.h.v de Scrum methode. Ik heb besloten om een ouder project van mij,
Betlej Elektrotechniek, een moderne uitstraling te geven. Ik heb daarom een plan van aanpak geschreven waar ik de rest van de middag mee bezig ben geweest. Rick gaat hier volgende week naar kijken om feedback op te geven.

8 uur gewerkt vandaag, 40 uur totaal deze week.
