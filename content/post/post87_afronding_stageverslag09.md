---
title: Afronding stageverslag 0.9
date: 2020-01-07
---

Vandaag alleen maar gewerkt aan het stageverslag. Met Rick afgesproken dat ik morgenochtend versie 0.9 aan hem oplever voor feedback. Daarom heb ik vandaag:
-	Alle resterende STARRTS afgemaakt
-	Bijlages verzameld en verwerkt in het verslag
-	Overzicht gemaakt van mijn leerproces met Daily UI’s

9 uur gewerkt vandaag.
