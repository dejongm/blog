---
title: Presentatie voorbereid + DUI20
date: 2019-12-11
---

Vandaag van Martin te horen gekregen dat Fabian, mijn tussentijdse stagebegeleider, de rest van deze en volgende week niet aanwezig is ivm werkzaamheden bij een ander ontwerpbureau. Daarom afgesproken dat ik nu mijn ontwerppresentatie in de middag ga oefenen met Gitva, Mohsen en Martin.

Door de ontvangen feedback van mijn stagebegeleider van school had ik ook een aantal vragen over het aantonen van mijn leerdoelen. Daarom Rick een berichtje gestuurd of ik hem in de avond kan bellen zodat hij mij kan helpen.

Ook heb ik met Mohsen afgesproken dat ik een paar persona’s ga maken voor de DUI van een locatie tracker. Voor deze DUI wilde ik iets anders proberen en mijn ontwerp zo onethisch mogelijk maken. Daarom heb ik een persona gemaakt van een ‘computernerd’ die zo anoniem mogelijk wilt zijn op het internet en een overheid die alles in de gaten wilt houden en zoveel mogelijk wilt weten over de gebruiker. Deze persona’s heb ik allebei met een verhaal uitgewerkt. Mohsen gaf de volgende feedback op mijn eerste versie:
-	Scheiding toevoegen tussen biografie en de ambities van de persoon
-	Voeg iets van een foto/leeftijd/baan toe zodat men kan relateren met de persona
-	Het stuk over de overheid leest nu als de plot van een boek i.p.v. een persona.

Ter voorbereiding van de ontwerppresentatie deze middag, heb ik nog het één en ander uitgewerkt voor de PowerPoint. Zo heb ik alle dia’s vernieuwd zodat het er uit ziet als een webpagina waar je doorheen scrolt. Ook heb ik een spiekbrief uitgewerkt en de presentatie minder tekstueel gemaakt. Aan het eind van de middag heb ik deze presentatie geoefend in de vergaderruimte. Daar heb ik de volgende feedback op ontvangen:
-	Door schuttingtaal verklein je wat je hebt gedaan terwijl dit helemaal niet nodig is
-	Minder met handen praten, dit leidt af
-	Geeft context wat de aanleiding is van deze opdracht
-	Sluit je presentatie af met wat de uitkomsten zijn van dit project

7,5 uur gewerkt vandaag.
