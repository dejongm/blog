---
title: Onderzoek AH afgerond
date: 2019-12-09
---

Vandaag gelijk weer aan de slag gegaan met mijn onderzoek naar de Albert Heijn. Hiervoor heb ik vandaag uitgewerkt:
-	Onderzoek naar privacybeleid concurrentie
-	Het privacybeleid & aanmeldproces van de Albert Heijn onderzocht en dark patterns gedocumenteerd
-	Conclusie uitgeschreven
-	Aanbevelingen gemaakt
-	Bronnenlijst volgens APA-regels gemaakt

8 uur gewerkt vandaag.
