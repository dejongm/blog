---
title: Begin aan PechaKucha + feedback verwerking ABR + wireframes BE
date: 2019-10-11
---

Vandaag heb ik in de ochtend een eerste opzet gemaakt voor mijn PechaKucha presentatie. Ik heb 20 slides nodig, waarvan ik er nu ongeveer de helft had uitgedacht.

Kort hierna kwam Mike naar mij toe met de vraag of ik de social-mediacampagne voor ABR wil updaten. Deze klant is naar ons toe gekomen met de vraag of wij de copy van eerder opgeleverde campagnes willen bijwerken en een paar kleine visuele wijzigingen doorvoeren.

Daarom heb ik een nieuwe mail banner gemaakt en een poster, mail header en facebook plaatje bijgewerkt met nieuwe tekst. Vervolgens heb ik de verbeteringen aan Mike laten zien, die vervolgens de klant de nieuwe versies heeft aangeleverd. De klant was er gelukkig erg tevreden mee.

In de middag heb ik weer aan mijn eigen project gewerkt. Ik ben voornamelijk bezig geweest met mijn schetsen verder te digitaliseren. Ook heb ik meerdere versies gemaakt voor een hero, geleverde diensten en contactpagina. Tevens heb ik Trello bijna volledig bijgewerkt en overal subtaken aan toegepast zoals ik heb geleerd van Rick. Aan het eind van de middag heb ik een aantal schoolwerkzaamheden verricht.

8 uur gewerkt vandaag, 32 uur totaal deze week (1 dag ziek).
