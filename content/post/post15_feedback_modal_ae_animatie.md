---
title: Feedback Modal ABR + presentatie + AE animatie
date: 2019-09-19
---

In de ochtend allereerst een standup meeting gehad. Hier voortgang project vertelt. Vervolgens aan de slag gegaan met ABR. Gelijk kwam Rick naar mij toe met de vraag hoe het ging met de modal die ik heb gemaakt. Vervolgens hebben we samen wat verbeteringen doorgevoerd:
-	 Afbeelding en tekst nu gescheiden van elkaar zodat het makkelijker leest voor de gebruiker
-	Emailadres van de rest van content gescheiden
-	Let op stukje nu onder de modal gezet
-	Nog wat kleine aanpassingen in kleurgebruik
-	Tekst ingekort

Vervolgens was aan mij gevraagd om UX-elementen te ontwerpen voor het invulformulier. Hiervoor heb ik dan ontwerpen gemaakt voor validatie, hover state, active state en foutmelding. Hiervoor heb ik verschillende versies uitgewerkt. Tevens ook een modal gemaakt voor de privacy statement.

Hierna hebben wij de voortgang met deze lander gepresenteerd aan de developers. Hier heb ik kort toegelicht wat ik heb gemaakt en hoe dit geïmplementeerd kan worden. Tijdens dit overleg heeft Rick aan mij gevraagd of ik mijn gemaakte illustratie wil proberen te animeren. Is niet verplicht maar meer als extra uitdaging.

Hierna ben ik dan ook begonnen met het maken van schetsen voor de illustratie. Ik heb ook wat filmpjes zitten kijken over After Effects en heb een simpele animatie gemaakt met een tutorial.

Rond half 5 was het alweer tijd voor een stage overleg met Rick over mijn voortgang. Hier hebben we mijn voortgang besproken en hoe het momenteel gaat op stage.

8 uur gewerkt vandaag.
