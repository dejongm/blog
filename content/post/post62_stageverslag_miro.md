---
title: Stageverslag + Miro inrichten
date: 2019-11-15
---

Vandaag heel de dag gewerkt aan het stageverslag. Ik heb besloten voor het concept verslag vier leerdoelen uit te werken:
-	Verdieping UX
-	Scrummen
-	Ontwerpproces
-	Presenteren

Rick gaf mij het advies om voor iedere STARRT eerst in steekwoorden uit te leggen waar je het over wilt hebben. Vervolgens deze steekwoorden vertalen naar korte zinnen. Hier heb ik mij in de ochtend mee bezig gehouden. Hierdoor heb ik nu een handig overzicht met wat ik in iedere STARRT wil bespreken zonder te veel moeite.

In de middag beziggehouden met al mijn papieren bewijsmaterialen te sorteren en in te scannen. Tevens besloten om alle bewijslast in een overzicht op Miro te verwerken. De rest van de middag bezig geweest om dit te doen voor het leerdoel van de verdieping in UX.

8 uur gewerkt vandaag, 40 uur totaal deze week.
