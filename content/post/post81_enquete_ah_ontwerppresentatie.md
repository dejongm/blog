---
title: Enquête AH uitgewerkt + ontwerppresentatie
date: 2019-12-12
---

Gisterenavond met Rick gesproken. Besloten mijn onderzoek naar de AH om te vormen naar de competentie empathie. Hiervoor wil ik verschillende persona’s uitwerken die de doelgroep en stakeholders van de Albert Heijn reflecteren. Daarom ga ik een enquête houden aangezien ik niet veel tijd meer heb. Hiervoor heb ik een aantal vragen opgesteld over:
-	Hoe veilig ze zich voelen bij de AH
-	Of ze een bonuskaart gebruiken
-	Of ze het privacybeleid hebben gelezen
-	Of ze deze dark patterns herkennen

Deze vragen heb ik besproken met Marieke besproken sinds zij in het verleden regelmatig onderzoek heeft verricht tijdens haar opleiding. Ze gaf als feedback:
-	Gebruik typeform, is een makkelijke dienst voor mooie enquêtes die er professioneel uit zien
-	Minder open vragen, hierdoor haken mensen af
-	Gebruik stellingen zoals “Ik voel me bekeken door de Albert Heijn” zodat mensen kunnen aangegeven of ze het hier mee eens zijn.
-	Je kan beter naar de voornaam vragen ipv alleen naam

Deze feedback verwerkt voor een nieuwe versie. Marieke gaf nu als feedback dat de vragenlijst nu te lang is en dat ik duidelijk moet hebben wat ik wil bereiken met de vragenlijst. Daarom heb ik het stuk over dark patterns geschrapt, vergelijkingsvragen voor het privacybeleid toegevoegd en een keuzelijst van onderwerpen toegevoegd. Hierdoor is de vragenlijst een stuk korter geworden. Deze versie nog een keer met Mohsen en Gitva getest waardoor hij nu prima is.

Ook heb ik na de lunch mijn ontwerppresentatie gepresenteerd aan het hele RAAK RDAM team. Dit ging best wel goed. Hier de volgende feedback op ontvangen:
-	Voornamelijk positief
o	Kwam zelfverzekerd over wanneer ik over het vakgebied praatte
o	Feedback van gisteren verwerkt: rustig gepraat, heel weinig schuttingtaal, minder erge handgebaren
o	Ik keek mensen aan tijdens de vragen

-	Verbeterpunten:
o	Af en toe pauze om mensen aan te kijken tijdens de presentatie
o	Aanleiding presentatie was niet 1005 dudelijk
o	Was wellicht leuk geweest om de voortgang te laten zien
(meerdere versies ontwerp)

7,5 uur gewerkt vandaag. 
