---
title: Kickoff WeConnekt + feedback DUI’s
date: 2019-09-11
---

Vandaag zijn we de dag begonnen met een meeting over de WeConnekt app. Hier werd ik op de hoogte gebracht van de stand van zaken met dit project. De WeConnekt app is een manier voor vrachtwagenchauffeurs om vracht dat dezelfde kant op moet zo veel mogelijk te delen. Zo kan men voorkomen dat vrachtwagens half leeg rijden en op deze manier kan er ook brandstof bespaard worden.

Momenteel ziet deze app er nog ouderwets uit. Aan RAAK de vraag om deze app van een nieuwe huisstijl te voorzien. Fabian heeft hier al het een en ander voor uitgewerkt. De huisstijl en eerste opmaak bestaat dus momenteel al. Deze vergadering was voornamelijk bedoeld om iedereen op de hoogte te brengen van de stand van zaken van dit project. Deze eerste opzet moet nog naar de opdrachtgever verstuurd worden ter goedkeuring. Aan mij werd gevraagd om hier naar te kijken en of ik al een aantal schermen verder wil uitwerken.

Na deze vergadering ben ik aan de slag gegaan met het doorlezen van documentatie over Scrum, ter voorbereiding van een leerdoel van mij. Ook kwam Rick, mijn stagebegeleider, naar mij toe om feedback teg even op twee daily UI challenge die ik heb gemaakt.

Als feedback op DUI1 heb ik gekregen:
-	Koppen beter uitlijnen met header
-	Werk ook UX-elementen uit: hover, active en feedback state
-	Mogelijkheid om wachtwoord in te zien tijdens typen
-	Welcome titel betere plek geven, is nu niet gelijk duidelijk
-	Venster om te registeren groter dan foto, sinds dat je hoofdfocus is
-	Registerknop meer aanwezig maken, fellere kleur
-	Lost details knopje
-	Bolletjes wachtwoord kleiner

Feedback DUI2:
-	Knop van size meer afscheiden van checkout knop
-	Checkout knop breder zodat je overal 20px afstand hebt
-	Knop van size compacter maken + EU42 weghalen, kan verwarrend zijn met prijs
-	Meer spacing tussen letters (Minimaal een letter afstand er tussen)
-	Je gebruikt bolletjes voor foto’s, maak hier streepjes van. Anders haal je apple en material design door elkaar
-	Zorg voor afscheiding tussen cc-nummers zodat het makkelijker leesbaar is
-	Bij checkout scherm lettertype groter van wat je invoert. Er moeten zo min mogelijk drempels zijn voor de persoon die afrekend
-	Expiration date volledig in cijfers, dit heb je overal
-	Tekst OP de regel. Dichter op de lijn, meer afstand tussen titel en invoertekst.
-	Pay now aanpassen naar pay. Ook knop een andere kleur geven sinds het een definitieve actie is.

Ik heb alle feedback genoteerd en ben begonnen met dit aan te passen in DUI1. Tevens kwam Alex naar mij toe met de vraag om een lijst met leads te sorteren in een Excelsheet. Hier ben ik de rest van de dag mee bezig geweest. 8 uur gewerkt vandaag.
