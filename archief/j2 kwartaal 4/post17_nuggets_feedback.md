---
title: Nuggets + feedback
date: 2019-06-14
---

Vandaag hebben wij a.d.h.v. onze testresultaten nuggets gemaakt. Op deze manier hebben wij in een handige lijst alle inzichten uit onderzoek en feedback verwerkt in actiepunten voor het prototype.

Ook zijn we als groep naar Marije gegaan om feedback te vragen op onze conceptrichting. Zij vond het een goede toevoeging om iets van een smartdevice te koppelen. Ze merkte wel op dat we nu een iets te brave richting opgaan met het project. De benaming van de categorieën mag spannender en de huisstijl mag ook meer een eigen smoel hebben.
