---
title: Nieuwe kwartaal
date: 2019-05-07
---

Het was vandaag weer de eerste dag na een lange vakantie. Na een korte uitleg over onze sprint debriefing zijn we aan de slag gegaan met het concept. Ik heb voorgesteld om een MoSCoW-analyse te maken om af te bakenen waar we op willen focussen met ons concept.

Tevens kregen we vandaag te horen dat we aanstaande vrijdag een conceptposter aan Ijsbrand kunnen gaan presenteren, daarom hadden wij een planning gemaakt over wie wat doet voor de poster. Leroy en ik houden zich bezig met de vormgeving terwijl Jari en Emma voor de inhoudelijke tekst zorgen.
