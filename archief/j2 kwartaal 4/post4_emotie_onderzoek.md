---
title: Emotie onderzoek
date: 2019-05-16
---

Met ons concept hebben bij besloten vol op emotie te gaan zitten. Ik heb daarom alvast wat onderzoek verricht naar hoe je emotie kan aanspreken binnen het ontwerp. Ik heb een hoofdstuk uit Design of Everyday Things gelezen waar ik achter kwam dat er drie niveau’s zijn van emotie aanspreken: Visceral, behavorial en de reflective level. Ook heb ik onderzocht op wat voor manieren je emotie en kleur kan aanspreken.
