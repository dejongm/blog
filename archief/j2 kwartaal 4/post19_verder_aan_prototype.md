---
title: Verder aan prototype
date: 2019-06-18
---

Ik had gisteren dan een eerste versie van het nieuwe Framer prototype gemaakt. Ik heb dit aan de rest van het team laten zien maar zij miste toch de kleuren die wij in eerdere versies van het prototype hadden. Daarom ben ik weer terug naar de schetstafel gegaan.

We hebben verschillende versies gemaakt en uiteindelijk zijn we tot nieuwe ideeën gekomen voor het overzicht van de verschillende moods. Ook hebben we afgesproken dat we nu alle categorieën volledig gaan uitwerken met drie artikelen per categorie.
De rest van de dag ben ik bezig geweest met het verwerken van alle artikelen en heb ik een tutorial toegevoegd aan het begin van het prototype.  
