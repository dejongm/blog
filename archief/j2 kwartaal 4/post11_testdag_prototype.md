---
title: Testdag prototype
date: 2019-06-06
---

Vandaag hebben wij in de middag een testdag gehouden voor onze verschillende prototypes. Ik heb mijn eigen prototype getest, deze gebaseerd op kleuren.

Deze versie van het prototype heb ik samen met Jari getest. Als feedback hebben wij ontvangen:

-	Navigatie onderwerpen mag duidelijker, links en rechts swipen is niet aangegeven.
-	Merkt op dat bepaalde knoppen en categorieën nog niet werken. Irriteert zich hieraan.
-	Maakt een opmerking over dat hij de kleuren van de categorieën niet goed aansluiten.
-	Geeft aan dat afbeeldingen uitnodigender mogen en de titels meer pakkend. Dit is wat hem aantrekt om iets te lezen.
-	Gemaakte categorieën vindt Hanno grappig, hij vindt het ook een interessant concept.
-	Nacht modus is iets dat hij terug wilt zien in het concept.
-	Navigatie door categorieën mag beter.
