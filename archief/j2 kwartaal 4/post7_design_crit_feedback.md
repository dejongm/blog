---
title: Design crit feedback
date: 2019-05-24
---

Vandaag was er een design crit op school. Hier hadden wij de mogelijkheid om feedbak te krijgen op ons concept van oud CMD studenten.

Per feedback ronde hebben wij een korte uitleg gegeven over ons onderzoek, wat uit ons onderzoek is gekomen en hoe wij dat hebben verwerkt in een concept. Vervolgens hebben wij uitgelegd wat het concept precies inhoud en wat de mogelijke functies zijn.

Feedback Maarten Somers

-	Denk na over hoe je actueel nieuws brengt aan de gebruiker. Mensen zijn bang om nieuws te missen.
-	Je concept is op het gebied van doelgroep nog gebaseerd op aannames. Onderzoek of 30+ers bereid zijn om voor het NRC te betalen.
-	Je hebt een goed concept maar er is nog niet getest. Kijk of je verschillende scenario’s kan uitwerken bij je prototype om het te testen met de doelgroep.
-	Heeft gevraagd: Gaan mensen een bepaalde categorie kiezen? Wat voorkomt dat mensen in 1 afspeellijst blijven hangen?
-	Onderzoek: Hoeveel artikelen gaan mensen lezen in 1 sessie?
-	Kijk bijvoorbeeld naar Discover Weekly op Spotify, misschien kan je iets soortgelijks in het concept verwerken.
-	Je kan bijvoorbeeld docenten benaderen voor je onderzoek. Zij vallen ook onder de doelgroep.
 
Feedback Simon van Acht

-	Je concept idee is erg goed, maar nog niet heel ver uitwerkt. Ga verder het ontwerpproces in. Er zijn meerdere manieren om emotie aan te spreken, onderzoek wat voor richtingen er zijn en onderzoek met een doel.
-	Denk na over wat transparantie met AI betekent. De meeste mensen zijn niet geïnteresseerd in de technische werking. Ze zijn eerder bang voor fake news. Probeer een manier te verzinnen om mensen gerust te stellen op dit gebied.
-	Onderzoek hoe Spotify AI verwerkt in hun applicaties, misschien dat dit interessante richtingen kan opleveren.
-	Gaf als tip: Ga vol op emotie zitten, dit is een unieke richting. Kijk hoe je emotie kan aanspreken op het gebied van tone of voice, kleurgebruik of lettertypes.
-	Probeer iets te doen met jullie motto: Voor ieder moment.
-	Verder hebben ze nog ideeën gegeven voor afspeellijsten en emotie aanspreken:
o	Wellicht kan je iets zoals Philips Hue in de achtergrond verwerken
o	Denk na over wat je wilt communiceren met benamingen van afspeellijsten
o	Een andere kleur op basis van omgeving: bijvoorbeeld tijdens regen of ’s avonds een andere kleur
o	Afspeellijsten zoals ontdek Rotterdam of ontdek de wereld

Aan de hand van deze feedback hebben wij besloten drie verschillende conceptrichtingen uit te werken die allemaal te maken hebben met emotie aanspreken:

-	Tone of voice
-	Kleur
-	Typografie
