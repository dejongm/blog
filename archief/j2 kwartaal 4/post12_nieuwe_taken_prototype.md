---
title: Nieuwe taken prototype
date: 2019-06-07
---

In de middag zijn we als groepje weer bij elkaar gekomen. Jari en Emma hebben een nieuwe manier van prototype testing aan Leroy en mij laten zien. Met deze methode kan je live meekijken wanneer iemand een prototype test, zie je zijn gezicht en kan je op een tijdlijn notities maken.

Ook hebben wij vervolgstappen beschreven voor de volgende iteratie van het prototype naar aanleiding van de tests van gisteren. We hebben uiteindelijk besloten verder te gaan met het kleuren prototype. Ik heb dan ook de taak op mij genomen om nieuwe kleuren te bedenken voor de categorieën die beter aansluiten en een tutorial maken. De rest van de taken zijn ook verdeeld: iedereen verzameld drie artikelen om te verwerken in de prototype categorieën.
