---
title: Testdag Leefstad
date: 2018-11-30
---

Vandaag was het tijd om te gaan testen in de stad. Op voorhand had Josephin een testplan uitgewerkt hoe we dit gaan aanpakken. Deze hebben we nu met z’n alle besproken. We leggen ons concept in het kort uit met de poster. Vervolgens laten we een filmpje zien van het scenario datr we hebben uitgwerkt. Ten slotte leggen we een kort interview af over ons concept.

We hebben twee uur door het centrum van Rotterdam gelopen waarbij we in totaal vier goede interviews hebben kunnen afleggen. Ik heb een wat oudere mensen gesproken waarbij ik een aantal goede inzichten heb verkregen. Dit waren o.a:

-	We moeten een manier verzinnen om gebruikers het concept te laten gebruiken. Bijvoorbeeld huurkorting
-	Er moet rekening gehouden worden met aansprakelijkheid bij ongevallen
-	Compensatie het liefst in natura
-	Bij voorkeur mobiel als medium
-	Sociaal contact is tegenwoordig steeds minder in de buurt. Hier is wel veel behoefte aan binnen de doelgroep
Dit heb ik vervolgens uitgewerkt in een document en gedeeld met de rest.
