---
title: Checklist expo + opbouwen proefexpo
date: 2019-01-18
---

We zijn de dag begonnen met een lijstje te maken wat nu allemaal nodig hebben voor onze eind expositie volgende week vrijdag. Deze is als volgt:

Vereist
- Mockups/affiches
- Flyer met sfeerimpressie (bushokje)
- Advertentie gebruik gebruiker
- 2x overige posters die we ophangen A3 incl. kernwaarden
- 2x Tafels fixen
- Scenariokaarten
- Huurcontract
- Laptop/telefoon
- Onepager
- Pen & papier
- Feedbackformulier

Aankleding
- Planten - jari
- Bluetoothspeaker + afspeellijst - lorenzo
- Tafelkleedje - michael
- 2x Karafje water - andor
- Stroopwafels - andor
- Voorverpakte koekjes/chocola - andor
- Bekertjes - michael

Ook hadden we in de middag de mogelijkheid om op de proefexpositie onze stand op te bouwen en zo nodig dingen aan te passen. Met alle aankleding erop en eraan hebben we denk ik een uitnodigde stand volgende week vrijdag.
