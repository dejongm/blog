---
title: Feedback prototype mobiel
date: 2019-01-11
---

Vandaag was het op school mogelijk om feedback te krijgen op je uitgewerkte prototype bij verschillende docenten. Aangezien Jari al een klikbare versie beschikbaar had van zijn mobiele prototype, hebben we deze gebruikt om feedback te krijgen op ons concept.

We hebben feedback gevraagd aan Rolf en Jantien gevraagd en we hebben heel veel goede feedback gekregen. Het is te veel om hier te vermelden maar de feedback is verwerkt en op onze dropbox geplaatst. Aan de hand van deze feedback hebben Jari en ik besloten welke we gaan verwerken in het prototype. Dit betreft o.a.:

-	Algemene voorwaarden pagina
-	Over Leefstad pagina
-	Meerdere klusjes/activiteiten
-	Speciaal voor jou uitlichten
  o Specialiteit + suggesties
  o	Tags
-	Voortgang op profiel over voltooide klusjesj
-	Uitgelicht carrousel
-	Wijzigen menu + footer op mobiel
-	Workshops + activiteiten bedenken + foto’s
