---
title: Bespreking persona, onderzoekstechnieken + rolverdeling
date: 2018-11-27
---

Vorige week hebben we met elkaar afgesproken om een eigen persona te maken zodat het voor iedereen duidelijk is wat zijn of haar kwaliteiten zijn en waar iemand zichzelf op wilt verbeteren. In de vergadering van vandaag hebben we deze persona’s met elkaar doorgenomen. Ook hebben we in deze vergadering de gemaakte ontwerpproceskaart van Lorenzo besproken en een aantal verbeteringen gesuggereerd. Verder hebben we afgesproken dat we vanaf nu beter beschikbaar gaan zijn op school, i.p.v. 3 dagen zijn we nu 4 dagen per week aanwezig op school.

Na de vergadering hebben we onze verzamelde fieldresearchtechnieken met elkaar gedeeld. Iedereen in het team had drie technieken verzonnen en vervolgens hebben we de lijst gefilterd tot we drie technieken hadden die we willen gebruiken om onze doelgroep verder te onderzoeken. Eigenlijk kwamen we hierdoor tot een dilemma omdat we het gevoel hadden dat we weer opnieuw begonnen met onderzoeken terwijl dit helemaal niet nodig was. Daarom hebben we gevraagd aan Ellen of zij ons hierover advies wilde geven. Door met Ellen te praten zijn we tot de conclusie gekomen dat we beter met ons favoriete concept (Leefstad) de straat op kunnen gaan en dit te testen met de doelgroep. Dit willen doen in de vorm van een filmpje + conceptposter. Hierna hebben we een planning gemaakt voor de komende twee weken.
