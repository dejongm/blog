---
title: Beschrijving concept + visual styleguide
date: 2018-12-04
---

In de ochtend zijn we bij elkaar gaan zitten en hebben we concreet gemaakt wat we nu in ons concept gaan uitwerken. We hebben aan de hand van onze opgestelde ontwerpcriteria categorieën ontworpen en de verschillende functies beschreven op papier. Ik werk dit later verder uit in een document.

Vervolgens ben ik verder aan de slag gegaan met de visual styleguide. Hierbij ik o.a. bepaald hoe ons logo wordt gebruikt en welke kleuren en foto’s we gebruiken incl. onderbouwing.

In de middag had ik visual lab, wat ging over het gebruik van lettertypes. Hier kreeg ik uitleg over hoe een lettertype is opgebouwd, wat voor categorieën er zijn en hoe je bepaalde lettertypes kan inzetten om een bepaalde indruk te geven.
