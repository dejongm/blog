---
title: Terugblik kwartaal 2
date: 2019-01-21
---

Deze keer ging de samenwerking een stuk beter dan vorig kwartaal. We zijn nu wat meer op elkaar ingespeeld en de sfeer is nu beter. En omdat iedereen een persona over zichzelf had gemaakt, wisten we waar ieders kwaliteiten en kansen liggen. Wat onze samenwerking ook veel heeft geholpen is dat we nu eindelijk structuur hadden door een standaard opening van de dag en een wekelijkse vergadering. In deze vergaderingen bespreken we waar iedereen mee bezig is, wat er nog gedaan moet worden en als iemand wat wilt delen met de groep. Dit gaf ons veel meer houvast tijdens het kwartaal.

Ik wilde me graag verdiepen in visual design dus heb ik die rol op mij genomen dit kwartaal. Ik heb verschillende visual design labs gevolgd waarbij ik voornamelijk heb geleerd dat niet alles in 1x gemaakt is. Je maakt heel veel verschillende iteraties en daaruit kies je de beste die je verder wilt uitwerken. Dit heb ik dan ook toegepast met verschillende deliverables die ik heb gemaakt. Ook heb ik in deze labs geleerd hoe ik een huisstijl kan maken voor Leefstad.

Allereerst was het kwartaal begonnen met een nieuwe sprint briefing. We hebben momenteel als doelgroep sociale huurders van middelbare leeftijd gekozen, maar na dit besluit hebben we geen onderzoek meer verricht naar de doelgroep.

Daarom hebben we als eerste in kaart gebracht wat we nu wilden weten van de doelgroep voor ons onderzoek. Hier is o.a. uitgekomen dat we de kenmerken van de doelgroep willen weten, welke communities momenteel actief zijn in Rotterdam en wat voor platform wij willen gebruiken voor ons concept. Hierna hebben we een aantal verschillende fieldresearch technieken verzameld. Iedereen in het team had drie technieken verzonnen en vervolgens hebben we de lijst gefilterd tot we drie technieken hadden die we willen gebruiken om onze doelgroep verder te onderzoeken.

Eigenlijk kwamen we hierdoor tot een dilemma omdat we het gevoel hadden dat we weer opnieuw begonnen met onderzoeken terwijl dit helemaal niet nodig was. Daarom hebben we gevraagd aan Ellen of zij ons hierover advies wilde geven. Door met Ellen te praten zijn we tot de conclusie gekomen dat we beter met ons favoriete concept (Leefstad) de straat op kunnen gaan en dit te testen met de doelgroep. Dit hebben we gedaan in de vorm van een filmpje en een conceptposter.

We hebben twee uur door het centrum van Rotterdam gelopen waarbij we in totaal vier goede interviews hebben kunnen afleggen. Ik heb een wat oudere mensen gesproken waarbij ik een aantal goede inzichten heb verkregen. Dit waren o.a:

-	We moeten een manier verzinnen om gebruikers het concept te laten gebruiken. Bijvoorbeeld huurkorting
-	Er moet rekening gehouden worden met aansprakelijkheid bij ongevallen
-	Compensatie het liefst in natura
-	Bij voorkeur mobiel als medium
-	Sociaal contact is tegenwoordig steeds minder in de buurt. Hier is wel veel behoefte aan binnen de doelgroep
Op basis van deze inzichten en dat van de andere interviews hebben wij ontwerpcriteria opgesteld die wij hebben gebruikt voor het verdere ontwerp van ons concept. We hebben vervolgens concreet gemaakt welke functies wij willen toevoegen aan Leefstad en dit ook gekoppeld aan de behoeftes en waardes van de doelgroep. Dit is dan ook beschreven in de conceptbeschrijving en onderbouwing. Ook heb ik voor Leefstad een huisstijl gemaakt en dit verwerkt in de visual styleguide die wij gebruiken voor het prototype en onze documentatie.

Wat wij tot nu toe hadden bedacht hebben we gepresenteerd aan een medewerker van Woonstad en Bob. Zij waren enthousiast over onze presentatie en hebben goede feedback gegeven.

Aan de hand van deze feedback en onze ontwerpcriteria zijn we aan de slag gegaan met het daadwerkelijk uitwerken van een prototype. We hadden afgesproken dat Jari het mobiele prototype maakt en ik de desktopversie. Josephin heeft wireframes voor ons allebei gemaakt.

Ook was er een moment beschikbaar om feedback te krijgen op ons concept. Aangezien Jari al een klikbare versie beschikbaar had van zijn mobiele prototype, hebben we deze gebruikt om feedback te krijgen op ons concept.

We hebben feedback gevraagd aan Rolf en Jantien gevraagd en we hebben heel veel goede feedback gekregen. Het is te veel om hier te vermelden maar de feedback is verwerkt en op onze dropbox geplaatst. Aan de hand van deze feedback hebben Jari en ik besloten welke we gaan verwerken in het prototype.

Momenteel is de desktopversie van het concept bijna af, deze moet nog alleen klikbaar gemaakt worden voor de expositie aankomende vrijdag. Ook hadden we afgelopen week de mogelijkheid om op de proefexpositie onze stand op te bouwen en zo nodig dingen aan te passen. Met alle aankleding erop en eraan hebben we denk ik een uitnodigde stand volgende week vrijdag. Al met al hebben we veel voortgang geboekt dit kwartaal en ben ik trots op het concept dat wij hebben bedacht en uitgewerkt.
