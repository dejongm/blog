---
title: Presentatie Woonstad
date: 2018-12-07
---

Vandaag was het tijd om onze voortgang met ons concept aan Woonstad en onze docent te presenteren. Aangezien ik gisteren de powerpoint presentatie had voorbereid wilde ik bespreken met de rest van het team hoe deze er uit ziet. Ik heb een aantal kleine aanpassingen gedaan en de benodigde materialen uitgeprint.

Ook hebben we met z’n alle een aantal keer de presentatie geoefend zodat we onder de knie krijgen wat we nou moeten zeggen. In de middag was het dan tijd voor de presentatie waarbij wij hebben uitgelegd waarom we voor Leefstad hebben gekozen, op voor ontwerpcriteria ons concept is gebaseerd en wat leefstad inhoudelijk is. Ook hebben we ons gemaakte filmpje over Leefstad laten zien.

Bob en de medewerker van Leefstad waren enthousiast over onze presentatie. We hebben o.a. de volgende feedback ontvangen:

-	Kijk hoe je het gaat marketen (wat is budget? Ga je kleinschalig?)
-	Op wat voor schaal wordt het concept? Buurt? Wijk? Stad?
-	Hoe ga je gebruikers stimuleren? Bijv met huurkorting maar wordt dat niet duur?
-	Als mensen naar woonstad bellen hoe ga je ze soepel naar leefstad sturen? Maken hun een melding aan? Leefstad helpdesk?

Deze feedback heb ik vervolgens verwerkt en op onze Dropbox gezet.
