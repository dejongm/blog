---
title: Kickoff summerschool
date: 2018-06-18
---

Vandaag om 13.00u is de kickoff van summerschool begonnen. Hier is mij verteld wat de planning wordt voor de komende twee weken. Deze twee weken wordt een pittige periode voor mij. Er wordt elke dag veel van mij verwacht en ik moet in een korte periode een prototype neerzetten. Verder is mij verteld wat de opdracht precies in gaat houden. Dit betreft het verzinnen van een concept voor een huurfietsensysteem dat past bij de wensen en behoeftes van de doelgroep.

Verder heb ik vandaag de taak gekregen om een doelgroep, implementeerlocatie en het bedrijf waar ik het voor ontwikkel te bepalen. Om dit te doen ben ik begonnen met het maken van een longlist. Hierbij heb ik bij elke categorie zo veel mogelijk passende keuzes opgeschreven. Vervolgens heb ik van deze lange lijst de meest interessante keuzes gemarkeerd. Dit heet een shortlist. Door de lijst met keuzes te beperken kan je makkelijker een keuze maken.

Uiteindelijk heb ik als doelgroep toeristen gekozen. Ik heb voor deze groep gekozen omdat toeristen een veelzijdige groep is met ieder zijn eigen mening en belangen. Ik heb Rotterdam als locatie gekozen omdat ik zelf in Rotterdam woon en ik ben benieuwd wat toeristen naar Rotterdam trekt. Als bedrijf heb ik Tripadvisor gekozen. Ik heb voor dit bedrijf gekozen omdat het hele bedrijfsmodel van Tripadvisor is gebaseerd op reizigers een goede vakantie te laten beleven. Ik ben benieuwd of ik dat kan door vertalen naar een passend huurfietsensysteem voor toeristen.
