---
title: Terugblik kwartaal 4
date: 2018-06-02
---

Dit was een zwaar kwartaal voor onze projectgroep. Ik ben het deze keer wel wat serieuzer gaan nemen dan kwartaal 3. Tijdens het maken van het nieuwe plan van aanpak heb ik gelijk een strakke planning gemaakt op Trello waarbij ik alle benodigde deliverables heb verzameld incl. subtaken, dit in fasen verwerkt en verdeeld onder ons groepje. Hierdoor hadden we gelijk een duidelijk overzicht van wat er van ons verwacht werd dit kwartaal.

Vervolgens hebben we doormiddel van deskresearch, observatie en interviews informatie verzameld over onze doelgroep en dit in een onderzoeksrapport verwerkt. Aan de hand hiervan hebben we een concept kunnen verzinnen dat aansluit bij de doelgroep. Dit concept hebben we vervolgens succesvol kunnen testen in de Tarwewijk en dit uitgewerkt in een testrapportage. Met deze gegevens hebben we het concept aangepast en uitgewerkt tot een high-fidelity clickable prototype.

De verschillende deliverables en het onderzoek hebben uiteindelijk toch meer tijd gekost dan dat we aanvankelijk hadden verwacht waardoor we het high-fidelity prototype niet 100% compleet hebben kunnen uitwerken en testen. Tijdens het proces heb ik hier wel rekening mee gehouden en onze planning hierop aangepast.

Al met al ben ik tevreden over het eindresultaat en ben ik erg blij met het team dat we hadden omdat we goed met elkaar konden opschieten. Ik ben nu zelfverzekerder geworden als ontwerper en ik kan nu mijn ontwerpkeuzes beter onderbouwen door het onderzoek dat ik heb verricht en de kennis die ik heb vergaard door het lezen van boeken, bezoeken van workshops en de design theory.
