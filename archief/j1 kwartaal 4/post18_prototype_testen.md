---
title: Prototype testen
date: 2018-05-24
---

Het testplan is gisterenavond volledig afgerond, dus vandaag heb ik het nieuwe testplan met het team doorgenomen zodat iedereen weet wat er moet gebeuren. Ook heb ik met Hanno nog kleine aanpassingen gemaakt aan het ontwerp.

Hanno heeft met Benny afgesproken, een klasgenoot van ons. Hij heeft een aantal maanden in de Tarwewijk gewoond dus dit paste in onze doelgroep. De test ging goed en we hebben leuke ideeën gekregen over ons concept.

Vervolgens zijn we met de metro naar de Tarwewijk gegaan. We hebben hier vier mensen kunnen interviewen, waarbij we iedere keer van rol hebben gerouleerd zodat iedereen daarmee kan oefenen.

Een aantal nuttige feedback die we hebben ontvangen is:

-	Uitleg is duidelijk om te begrijpen
-	Iedereen lijkt het fijner om de app gezamenlijk met andere mensen te gebruiken
-	Persoonlijke progressie werkt motiverend
-	Maak het competitief door highscores toe te voegen zodat mensen langer blijven spelen
-	Voeg een mogelijkheid toe om mensen toe te voegen
-	Maak icoontjes op de map duidelijker met een legenda
-	Opdrachten zijn van goed niveau
-	Een battle tussen wijken zou een leuk idee zijn
