---
title: Onderzoeksrapport
date: 2018-05-17
---

Ik ben vandaag begonnen met het uitwerken van het onderzoeksrapport. Ik heb wat extra deskresearch gedaan en dit verder uitgewerkt met extra bronnen. Alle interviews en bevindingen over de wijk verwerkt, foto’s toegevoegd en met al deze informatie conclusies getrokken over de wijk. De voornaamste dingen die hier uit zijn gekomen is dat:

-	Alle faciliteiten om gezond te leven zijn aanwezig in de wijk, maar men gebruikt deze niet
-	Veel jongeren hebben geen tot weinig regelmaat in hun leven
-	Jongeren zijn gevoelig voor meeloopgedrag en makkelijk beïnvloedbaar. 
