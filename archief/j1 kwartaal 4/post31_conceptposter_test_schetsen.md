---
title: Conceptposter, concepttest, schetsen en feedback
date: 2018-06-26
---

Ik ben vandaag aan de slag gegaan met de conceptposter, want die had ik nog niet. Dit heb ik aangepakt door eerst een paar schetsen te maken voor de indeling en wat er tekstueel in moet komen staan. Hierbij heb ik een illustratie in het midden gezet waar de aandacht van de lezer naar toe getrokken wordt, vier secties toegevoegd die de functies uitleggen met onderbouwing vanuit mijn onderzoek en een samenvatting van het concept.

Dit heb ik vervolgens verder uitgewerkt in Illustrator. Toen deze eenmaal uitgewerkt was ben ik mijn concept gaan testen bij klasgenoten. Hierbij heb ik vragen gesteld over de duidelijkheid van het concept, of het concept meerwaarde heeft voor de doelgroep, of er iets ontbreekt en vroeg ik naar visuele feedback. Ik heb hierbij een aantal leuke ideeën gekregen over functies die ik toe kan voegen aan het concept en hoe ik sommige functies beter kan aanpakken. Daarom heb ik besloten om een mogelijkheid voor groepen toe te voegen, chatfunctionaliteit en geef ik de gebruiker meer vrijheid in hun keuze tot fietsroute.

Vervolgens ben ik bij Mieke feedback gaan vragen over mijn conceptposter en verslag over creatieve technieken. De gemaakte conceptposter was in orde, alle belangrijke punten staan erin. De samenvatting van het concept moet iets concreter gemaakt worden en bovenaan gezet worden zodat je deze informatie als eerste leest. Mijn verslag over creatieve technieken was duidelijk, alleen moet ik wel een betere scheiding tussen verschillende onderdelen en de resultaten van iedere gebruikte techniek. Verder moet ik ook vermelden hoeveel personen, wie hebben meegewerkt en de tijdsduur van de creatieve sessie.

Met de feedback over de conceptposter ben ik gelijk aan de slag gegaan, dit was niet zo veel werk en heb ik aangepast. Ik heb bij Jantien ook weer feedback gevraagd op mijn debriefing nadat ik haar feedback heb verwerkt. Volgens Jantien was de debriefing nu in orde en goed genoeg om op te nemen in mijn leerdossier.

Thuis ben ik begonnen met het maken van schetsen voor mijn low-fidelity prototype. Hierbij heb ik mijn functies uit de conceptposter uitgewerkt en de verkregen feedback bij mijn test verwerkt in de functies.
