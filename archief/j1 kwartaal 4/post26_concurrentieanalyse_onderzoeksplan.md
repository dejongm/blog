---
title: Concurrentieanalyse + onderzoeksplan
date: 2018-06-19
---

Allereerst heb ik een presentatie gekregen over wat er van mij verwacht wordt vandaag. Hierbij is mij duidelijk geworden dat ik concurrende huurfietsensystemen moet onderzoeken om te bekijken hoe hun het aanpakken. Aan de hand van deze bevindingen moet ik een ontwerpdoel en onderzoeksplan gaan vaststellen zodat ik mijn eigen doelgroep kan onderzoeken.

Ik ben begonnen met vergelijkbare bedrijven te onderzoeken. Dit heb ik gedaan met de ov-fiets, mobike, go-bike en obike. Hierbij ik met deskresear h gekeken naar de verschillende touchpoints, prijzen, betaalsysteem en hoe de gebruiker het systeem kan gebruiken. Ook heb ik op internet gezocht naar bekende problemen met de systemen zoals overlast en de gebruikerservaring. Vervolgens heb ik via fieldresearch mobike onderzocht. Ik heb de app van hun gedownload, een account aangemaakt en een fiets gehuurd door het scannen van de QR-code op de fiets. Ook heb ik van het hele proces foto's gemaakt. Vervolgens heb ik een stukje gefietst en mijn bevindingen daarvan opgeschreven i.c.m. de deskresearch. Dit alles heb ik verwerkt in een klein onderzoeksrapport van mijn concurrentieanalyse.



De bevindingen van mijn groep en ik, heb ik vergelijkt met een ander groepje. Omdat we een vergelijkbaar bedrijf hebben onderzocht, waren veel bevindingen hetzelfde. Wel was bij de andere groep de borg een stuk hoger en klopte de locatie van de fietsen niet helemaal met de aangegeven locatie in de app.

Aan de hand van de concurrentieanalyse heb ik een ontwerpdoel gevormd voor mijn doelgroep en eisen vastgesteld waar het concept aan moet voldoen. Vervolgens heb ik een onderzoeksplan gemaakt aan de hand van mijn ontwerpdoel. Ik heb hoofd en subvragen geformuleerd om het ontwerpdoel op te lossen, de huidige situatie beschreven, onderzoeksmethoden vastgesteld en verteld hoe ik mijn onderzoeksresultaten meetbaar ga maken. Voor het vaststellen van de onderzoeksmethoden heb ik het boek Universele Ontwerpmethoden gebruikt. In dit boek heb ik gezocht naar bruikbare onderzoeksmethoden die ik kan gebruiken in mijn onderzoek naar de doelgroep. Uiteindelijk heb ik gekozen voor semigestructureerde interviews, observaties en een merkanalyse.

Ik heb bij Robin feedback gevraagd op mijn ontwerpdoel. Robin heeft mij vertelt dat mijn ontwerpdoel te concept gericht is, ik moet het breder definiëren en pas vanuit mijn onderzoek na denken over een concept. Het ontwerpdoel heb ik daarom aangepast.

Met het gemaakte onderzoeksplan kan ik morgen dan aan de slag wanneer ik mijn doelgroep ga onderzoeken.
