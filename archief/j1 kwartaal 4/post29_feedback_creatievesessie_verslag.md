---
title: Feedback ontwerpproceskaart + creatieve technieken sessie + verslag
date: 2018-06-22
---

In de ochtend heb ik gelijk feedback gevraagd op mijn gemaakte ontwerpproceskaart. Als feedback heb ik gekregen dat de ontwerpproceskaart duidelijk geformuleerd was en dat ik heb goed heb aangegeven waarom ik voor bepaalde technieken heb gekozen. Bij het stuk over verbredende onderzoek van de concurrentie was deskresearch en fieldresearch te vaag geformuleerd, dit moet ik aanpassen naar concurrentieanalyse. Ook moet ik voor inrichten ontwerpproces niet vergeten het ontwerpdoel aan te passen in de debriefing aan de hand van nieuwe onderzoek inzichten.  

Hierna ben ik aan de slag gegaan met een mindmap. Ik heb hiervoor gekozen omdat je hierdoor uit één onderwerp veel verschillende ideeën kunt verzinnen. Hierdoor kunnen er verbanden en ideeën ontstaan tussen verschillende onderwerpen waar je normaal nooit aan zou denken. Dit was ook een goede manier om mijn onderzoek inzichten over Rotterdamse toeristen in de mindmap te verwerken en nieuwe ideeën hierover te verzinnen die gerelateerd zijn.

Vervolgens ben ik aan de slag gegaan met de convergerende techniek “Secret voting”. Ik heb hierbij mijn verschillende conceptideeën op papier geschreven met een uitleg en nummering. Vervolgens heb ik iedereen een beoordelingsformulier gegeven met een aantal criteria en een vlak om een eigen mening te geven. Hierbij moet ieder groepslid bepalen welk idee ze het beste vinden en waarom ze dat vinden. Uiteindelijk is het conceptidee met de meeste stemmen de winnaar.

Vervolgens krijgt iedereen een beoordelingsformulier met criteria en een vlak om een eigen mening te geven. Hierbij moet ook ieder groepslid bepalen welk idee ze het beste vinden, wat ze vervolgens kunnen verdedigen in een groepsdiscussie. Het idee dat het meest is gekozen is het idee voor een huurfietsensysteem voor backpackers die alleen reizen en via een app medereizigers kunnen ontmoeten om samen de stad te ontdekken. Dit ga ik dan ook vervolgens uitwerken tot een conceptposter.

Het proces van alle creatieve technieken, inclusief het verloop, argumentatie van de keuze van technieken en foto’s heb ik uitgewerkt tot een verslag. Hierbij heb ik ook de verschillende conceptideeën en mijn uiteindelijke keuze vermeld.
