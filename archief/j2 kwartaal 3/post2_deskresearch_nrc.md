---
title: Deskresearch NRC
date: 2019-02-21
---

Vandaag heb ik samen met Leroy afgesproken om te beginnen aan het onderzoek naar NRC. We zijn allereerst begonnen met het opstellen van een paar onderzoeksvragen:

-	Wat zijn de huidige reviews van NRC?
-	Hoe brengt NRC het nieuws op dit moment, wat voor medium gebruiken ze?
-	Hoe doen andere bedrijven hun nieuws verspreiden?
-	Hoe personaliseren andere bedrijven de feed van hun gebruikers?
-	Gebruiken sommige bedrijven een abonnement? Waarom doet het ene bedrijf dat wel en de andere niet?
-	Wat is de huidige status van NRC? Hoe wordt het nieuws geconsumeerd door de gebruiker?
-	Waar ligt de voorkeur bij het verspreiden van nieuws?
-	Hoe komt het dat gebruikers op dit moment in een filter bubbel raken?
-	Hoe kan je iemand het beste verrassen op psychologisch gebied?
-	Hoe wordt machine-learning op dit moment toegepast bij bedrijven in het geval van personalisatie?

Aan de hand van deze onderzoeksvragen zijn we aan de slag gegaan met onze deskresearch. Hierbij heb ik gekeken op de website/app van het NRC, heb ik wat deskresearch verricht naar nieuwsconsumenten en heb ik ook gekeken hoe concurrerende kranten hun nieuws brengen aan de klant. Hier ben ik eigenlijk heel de middag mee bezig geweest waardoor ik uiteindelijk alle onderzoeksvragen heb kunnen uitwerken in een document.
