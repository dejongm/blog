---
title: Eerste Dag
date: 2019-02-19
---

Vandaag was de eerste dag van het nieuwe kwartaal. Het is nu weer een nieuw groepje dus dat was weer wennen. We begonnen de ochtend met een korte introductie over de planning, daarna hebben we binnen ons team de projectrollen verdeeld.

Ik heb besloten dit kwartaal aan de slag te gaan als prototyper. Ik heb hiervoor gekozen omdat ik me meer wil verdiepen in programmeren en hoe ik snel en op veelzijdige manieren prototypen kan inzetten om projectresultaten te presenteren. Vandaag hebben we even snel een planning gemaakt met wat we precies willen gaan onderzoeken in deze fase van het project.
