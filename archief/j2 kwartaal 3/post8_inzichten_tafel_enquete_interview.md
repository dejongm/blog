---
title: Inzichten Tafel, Enquête + Interview
date: 2019-03-19
---

In de ochtend was er een gezamenlijke opdracht met de 2A en 2B klas. Alle verschillende inzichten werden gecategoriseerd doormiddel van verschillende tafels. Ons groepje had het onderwerp doelgroep. Elke tien minuten ging je naar een andere tafel en kon je je inzichten delen met de persoon die aan die tafel zit, die deze inzichten vervolgens weer opschrijft. Ik ben langs verschillende tafels geweest en mijn inzichten gedeeld en ook weer wat geleerd van andere groepen hun inzichten.

Eerder vorige week heeft de rest van het team een enquête gehouden over nieuwsconsumptie. Mij werd gevraagd om hier een samenvatting van te maken en een conclusie te trekken uit de resultaten. Hier heb ik mij in de avond mee bezig gehouden. Ook heb ik de resultaten van mijn cultural probe testpersoon teruggevraagd.
