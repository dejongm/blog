---
title: Researchposter Presentatie
date: 2019-03-22
---

In de middag hebben we onze onderzoeksresultaten gepresenteerd aan de opdrachtgever. Dit hebben we gedaan doormiddel van een researchposter. We zijn voor de presentatie even bij elkaar gekomen om met elkaar door te nemen wie wat gaat presenteren. Hierbij ik besloten mijn eigen onderzoek te presenteren, dat over AI. De presentatie zelf ging prima, ik heb goed kunnen vertellen wat ik wilde vertellen. We hebben alleen voor de rest geen feedback ontvangen over onze presentatie, hij vond het wel goed zo en is daarna verder gegaan naar het volgende groepje.
