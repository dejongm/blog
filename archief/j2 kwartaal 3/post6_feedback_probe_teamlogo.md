---
title: Feedback Cultural Probe + Teamlogo
date: 2019-03-08
---

Tijdens de studio les begonnen we met een kleine vergadering over hoe wij de voortgang van het project en onze samenwerking vonden gaan. Leroy stelde vragen over hoe iedereen het vond gaan en of er nog bijzonderheden waren. In het algemeen gaat alles goed, we liggen op schema bij de planning en zijn nu druk bezig met ons onderzoek naar de gebruiker. De planning was eerst nog wat passief en we werkte niet echt met strenge deadlines of taken. Maar het gaat tot nu toe wel prima, maar moeten wel inzicht blijven houden over het project en de taken / deliverables over een langere periode.
Toen zijn we verder gegaan met ons eerste test voor de cultural probe, Jari's ma had hem ingevuld en feedback gegeven,

Feedback: de kolommen voor de dagboekpagina zijn te klein, het emotie stukje werd niet goed begrepen dat je daar een gezichtsuitdrukking moest tekenen, het vlak om te schrijven over hoezo dit artikel is te groot (die ruimte kan beter benut worden).

Deze feedback hebben we verwerkt door de dagboek kolommen groter te maken, waardoor de opdracht tekenen en vragen naar de andere pagina schuiven. Waar meteen de extra ruimte is benut die te groot was. Ook hebben we nog bij de uitleg van de opdrachten de tekenopdracht erbij gezet, die was vergeten. Na alles nog eens na gelopen te hebben en aangepast hebben we deze 5x geprint. We hebben ook 5 testpersonen van verschillende leeftijdscategorieën.

Ook werd aan mij gevraagd om een teamlogo te ontwerpen voor ons team Moon Emoji. Hierbij heb ik gekeken naar de bestaande emoji van de maan en hier mijn eigen draai aan gegeven. Dit heb ik vervolgens gedeeld met de groep en zij zijn er tevreden over.

Ik had ook het gevoel dat wij in ons onderzoek nog niet voldoende aandacht hadden besteed aan personalisatie door machine learning. Daarom heb ik voorgesteld dat ik hier zelf nog deskresearch over ga doen, wat ik volgende week af wil hebben.
