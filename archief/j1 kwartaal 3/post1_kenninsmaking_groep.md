---
title: Kennismaking groep
date: 2018-02-12
---

Vandaag zijn we in nieuwe groepjes opgedeeld voor het project in kwartaal 3 & 4. Ons groepje heet De Brave Borsten met Andor, Hanno, Simon en Indra. We hebben allereerst wat over onszelf verteld en wat onze sterke en zwakke punten zijn.

Vervolgens zijn we aan de slag gegaan met het plan van aanpak. We hebben dit gezamenlijk als groep gedaan door met elkaar alle vragen te beantwoorden. Vervolgens heb ik thuis de vragen verder uitgewerkt.

Ook hebben we een gezamelijke team SWOT gemaakt en hebben we een Google Drive opgezet en Trello voor een scrumboard. Hierna zijn we naar huis gegaan.
