---
title: Rapid prototyping & validatie lifestyle diary
date: 2018-03-21
---

Omdat we de afgelopen 2 dagen gezond hebben gegeten, hebben we aan de hand van gegevens en foto’s de toekomstige lifestyle diary kunnen maken. Hier zijn we heel de dag mee bezig geweest.

In de middag had ik een workshop rapid protoyping waarbij we een brug moesten bouwen tussen twee stoelen met plakband en papier. Hiervoor hebben we doelen moeten opstellen en zo snel mogelijk verschillende prototypes uitgewerkt.

Rond 15.00u had ik een validatiemoment voor de lifestyle diary. Deze is niet goed gekeurd en ik moet daarom terugkomen om de diary opnieuw te laten valideren. Als feedback heb ik gekregen dat onze lifestyle diary meer een verzameling is van informatie. We moeten alle gegevens interpreteren en daaruit een conclusie afbeelden. Ook moeten er meer verschillende mediaprikkels afgebeeld worden.
