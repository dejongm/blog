---
title: Lifestyle diary, prototype & testplan
date: 2018-03-26
---

Omdat we achter liepen met onze deliverables hebben we heel de dag hard doorgewerkt aan het project. Allereerst hebben Indra en ik aan de hand van de verkregen feedback de toekomstige lifestyle diary opnieuw gemaakt. We hebben hier conclusies aan toegevoegd en meer verschillende mediaprikkels.

Nadat de diary was afgerond, zijn we begonnen met brainstormen voor ons low-fidelity prototype. We hebben dit gestructureerd aangepakt. We hebben een timer gezet van 10 minuten en Hanno aangewezen als de persoon die alles noteert. Binnen deze periode mag iedereen roepen wat in zich opkomt zonder dat hierover geoordeeld wordt. Hanno plakt elk idee vervolgens met plaknotities op tafel.

Na de 10 minuten hebben we alle ideeën onderverdeeld in 4 categorieën: (niet) realiseerbaar normaal en (niet) realiseerbaar origineel.

Vervolgens hebben we dingen geschrapt die we niks vonden, zodat er per categorie 1-4 dingen overbleven. Hierna hebben we met elkaar gediscussieerd om te bepalen welk idee ons tof lijkt om verder uit te werken. Uiteindelijk zijn we gekomen op een app die je helpt routine aan te leren. Met een soort mealprep planner en agenda integratie.

Nadat de brainstormsessie was voltooid en we een idee hadden zijn we verder gegaan met het maken van een mindmap. Met de mindmap konden we bepalen wat voor functies we allemaal in de app wilde hebben.

De mindmap was achteraf een erg goed idee. We hadden nu een lijst verzamelt met functies die we wellicht in de app wilde verwerken. We wilde geen app die bloated was met allerlei verschillende dingen, dus hebben we met een MoSCoW-analyse bepaalt waar onze prioriteiten lagen.


Uiteindelijk hadden we 3 dingen bij must have gezet: eetplanner, mealprep en de kalender. Tevens hebben we in ons prototype ook schermen uitgewerkt voor beloningen/mijlpalen die de gebruiker kan verkrijgen.

Hierna zijn we allemaal aan de slag gegaan met de eerste schets van ons prototype. Iedereen heeft een categorie genomen en dit op zijn eigen manier uitgewerkt. Ik heb de schermen voor de mealprep gemaakt. Ik heb hiervoor schermen gemaakt waarin je eet voorkeuren kan aangeven, voor welk moment op de dag, kwantiteit, recepten overzicht en een recept zelf.

Nadat we allemaal klaar waren hebben we een begin gemaakt aan het testplan waarin we de hoofdlijnen hebben bepaald. Omdat het toen al 20.00u was, zijn we naar huis gegaan.
