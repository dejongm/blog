---
title: Gezond eten
date: 2018-03-19
---

We hadden vandaag officieel geen studiodag maar we zijn toch bij elkaar gekomen om verder te werken aan het project. Omdat we voor de toekomstige lifestyle diary foto’s van 2 gezonde dagen moesten verzamelen, hebben we met de groep afgesproken om morgen en overmorgen met ze alle gezond te eten. We hebben daarom een menu verzonnen met allerlei gezonde gerechten wat we allemaal volgen en na 2 dagen onze bevindingen hiervan delen.

I.v.m. research hebben we ook een aantal vragen over de gezond opgesteld en afgesproken 2 mensen te interviewen en te vragen om 2 dagen lang bij te houden wat ze eten op een normale dag.
