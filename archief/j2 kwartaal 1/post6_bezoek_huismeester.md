---
title: Bezoek Huismeester
date: 2018-09-20
---

Vandaag ben ik naar de locatie Noord/Oost geweest van Woonstad. Hier heb ik huismeester Dennis ontmoet. We zijn in de wijk Prinsenland naar verschillende appartement complexen geweest. Hier hebben we helaas maar 1 bewoner bezocht, dus ik heb de doelgroep niet vaak kunnen spreken. Ik ben door de huismeester wel veel zaken over de bewoners, Woonstad en de gang van zaken te weten gekomen waar ik veel aan heb. Het uitgebreide verslag van mijn bezoek ga ik in een apart document uitwerken.
