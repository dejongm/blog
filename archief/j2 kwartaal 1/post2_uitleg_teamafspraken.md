---
title: Uitleg & Teamafspraken
date: 2018-09-07
---

Vandaag hebben we van een docent wat meer duidelijkheid gekregen over de opdracht en wat er van ons verwacht wordt. Hierbij heb ik de debriefing en sprint briefing doorgelezen. Hier las ik wat elke discipline inhoud en welke deliverables daarbij horen. Ook weet ik nu wat de opdracht is van Woonstad.

Na de uitleg hebben we als projectgroep teamafspraken gemaakt. Hier hebben we richtlijnen vastgelegd over communicatie, deadlines en te laat komen. Ook hebben we met elkaar besproken welke rol iedereen aanneemt. Ik heb besloten om mij te focussen op interaction design.
