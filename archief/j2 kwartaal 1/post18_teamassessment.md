---
title: Teamassessment
date: 2018-10-11
---

We hadden het al een tijdje op de planning staan, maar vandaag hebben we eindelijk een teamassessment gedaan. Dit hebben we gedaan aan de hand van uitgeprinte formuliertjes waarbij je elkaar + en – geeft bij een aantal categorieën. Vervolgens kan je je mening op de achterkant van het formulier verder toelichten. Ik heb daar als feedback gekregen dat ik de teamleider rol goed op me neem, maar ik moet wat meer opdrachten delegeren i.p.v. alles zelf te doen. Ook moet ik in discussies wat zekerder zijn van mezelf en wat meer structuur toepassen zodat we niet te veel tijd verspillen.

Na het assessment hebben we onze inzichten van het fieldresearch met elkaar besproken. Hier kwamen we achter dat men niet heel erg tevreden is over Woonstad. Ze verwachten meer service van Woonstad i.p.v. alles zelf te moeten regelen. Ook waren de meeste probleemmeldingen afkomstig uit Rotterdam-Zuid en merkte we dat er lange wachttijden zijn wanneer je contact op wilt nemen met Woonstad.

I.c.m. ons eerdere onderzoek, hebben we daarom besloten onze focus te willen leggen op oudere mensen vanaf 50+. Wij hebben hiervoor gekozen omdat dit een algemene basis geeft voor een concept die gebaseerd is op de problematiek die wij hierboven hebben aangekaart. Ook is uit onderzoek gebleken dat oudere mensen vaak moeite hebben met het gebruik van nieuwe technologieën en het persoonlijke contact hierbij missen.

Hierna ben ik aan de slag gegaan met onze doelgroep zo volledig mogelijk te beschrijven in een nieuwe versie van de debriefing. Ook heb ik aan de hand van onze inzichten ons ontwerpdoel aangepast. Ons doel is om een vernieuwende, eenvoudige en persoonlijke ervaring te bieden voor sociale huurders boven de 50 jaar waar klantenservice centraal staat.

Voor dat we richting huis zijn gegaan, hebben we met elkaar afgesproken dat ieder voor zich drie creatieve technieken gaat voorbereiden die we morgen kunnen inzetten voor het verzinnen van een concept. Bij deze technieken moeten er twee divergerend zijn en één techniek convergerend.
