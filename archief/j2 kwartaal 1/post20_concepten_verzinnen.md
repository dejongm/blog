---
title: Concepten Verzinnen
date: 2018-10-15
---

Vandaag zijn we begonnen met een kleine sessie squigly birds om een ongedwongen sfeer te creëren in de groep. Dit is een soort oefening waarbij je allerlei krabbels op een vel papier zet en deze vervolgens pootjes geeft, een staart en een snavel. Hierdoor krijg je allemaal grappige vogeltjes. Het klinkt als een hele stomme oefening, maar als je eenmaal bezig bent is het erg leuk en het zorgt voor een lekkere sfeer in de groep.

Toen we eenmaal opgewarmd waren, zijn we begonnen met een negatieve brainstormsessie. Hierbij neem je een probleemstelling en verzin je allerlei ideeën om het probleem te verergeren. Al deze ideeën schrijf je vervolgens op post-it notes en plak je op tafel. Na tien minuten hadden we best wel veel slechte ideeën kunnen genereren. Vervolgens hebben wij al deze slechte ideeën omgedraaid naar goede ideeën en oplossingen voor het problemen. Hier hebben wij een aantal bruikbare oplossingen uit kunnen halen.

We hadden nu door brainwriting en negatief brainstormen een aantal ideeën en oplossingen kunnen bedenken. We hebben dit geconvergeerd door een COCD-box te maken. Hier hebben wij verschillende ideeën gecategoriseerd. Het doel was om een aantal originele, realiseerbare ideeën te verkrijgen. Uiteindelijk hebben we gekozen voor een control panel, een soort community van de bewoners en een virtuele assistent in de vorm van een hologram. We wilden deze concepten verder uitdiepen, dus hebben we dat gedaan door het maken van een mindmap. Uiteindelijk zijn dit onze resultaten:

Control Panel

-	Tablet dat in elk huis hangt
-	Gemakkelijk meldingen aanmaken
-	Functies om het huis te bedienen (smarthome)
-	Gelijk een medewerker spreken of een afspraak maken voor de balie
-	Toegankelijkheidsopties voor oudere mensen

Virtuele Assisstent

-	Hologram van een Woonstadmedewerker die oproepbaar is
-	Vragen stellen over woning
-	Afspraak maken voor reparatie

Leefstad Rotterdam

-	Community van wijken die van Woonstad huren
-	Workshops organiseren (tuinieren, koken, reparaties)
-	Samen afspreken om met elkaar te koken
-	Bruikbaar op app/website
-	Elkaar helpen met reparaties voor vergoeding

We waren allemaal tevreden met de resultaten die we vandaag hadden geboekt. Als laatste hebben we een aantal taken verdeeld voor de verdere uitwerking van de concepten. Ik heb daarom samen met Andor de taak op mij genomen om het control panel verder uit te werken. We willen hiervoor schetsen maken en dit vervolgens op een barebone manier uitwerken in Adobe XD. Alleen het noodzakelijke om de functionele aspecten clickable te maken. Het maken van deze taakverdeling was het laatste dat we vandaag hebben gedaan. 
