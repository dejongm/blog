---
title: Concept Ideevorming
date: 2018-10-12
---

Allereerst hebben we vandaag een aantal creatieve technieken gekozen die wij willen gaan doen vandaag. Hierbij hebben wij gekozen voor brainwriting, download your learnings, superhelden, negatief brainstormen en mindmappen.

We zijn hierbij begonnen met download your learnings. Dit is een creatieve techniek waarbij iedereen in het groepje om de beurt vertelt welke inzichten hij heeft gehaald uit zijn onderzoek. Dit hebben we vervolgens in steekwoorden opgeschreven op aparte post-it notes en per categorie op een vel papier geplakt. Door deze techniek hadden we al onze inzichten makkelijk bij elkaar staan zodat wij dit op ieder moment konden raadplegen in de ideevormingsfase.

Toen we hiermee klaar waren zijn we begonnen met de techniek brainwriting. Hierbij krijgt iedereen een individueel blaadje waar hij of zij twee minuten de tijd heeft om zo veel mogelijk passende ideeën op te schrijven. Nadat de timer is verstreken wordt elk stuk papier met de klok mee doorgegeven. De volgende persoon in de groep borduurt de komende twee minuten dan voort op de opgeschreven ideeën. Je gaat dan net zo lang door totdat iedereen de groep elkaars papier heeft gehad. Vervolgens worden alle verzonnen ideeën op post-it notes geplakt.

Toen we eenmaal klaar waren, was het tijd voor een presentatie door Woonstad. Hier hebben wij informatie gekregen over onderzoek dat Woonstad zelf heeft verricht over zijn klanten. Hierbij zijn een aantal persona’s opgesteld en aan ons gepresenteerd. Ook hebben wij in dit uur tijd gehad om vragen te stellen aan Woonstad.

Na de presentatie hebben we nog tijd gehad voor één laatste creatieve techniek. Deze techniek heet superhelden. Hierbij schrijf je de probleemstelling op in het midden van het bord. Vervolgens kiest iedereen in de groep een superheld en moet je toelichten hoe deze superheld het probleem gaat oplossen. Vervolgens doe je hetzelfde weer, maar dan met de omgekeerde karakteristieke van de superheld. Dit was een vrij lastige techniek omdat we moeite hadden om bruikbare oplossingen te verzinnen met deze techniek. We hebben hier uiteindelijk ook niet zo veel uit kunnen halen.
