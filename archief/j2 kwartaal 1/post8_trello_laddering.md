---
title: Trello Opgeruimd & Laddering Interview
date: 2018-09-24
---

Trello werd niet actief bijgehouden, daarom heb ik de taak op mij genomen om dit vanaf nu te doen. Ik heb alles op een rij gezet wat we nu precies af moeten hebben voor de poster. Deze taken heb ik vervolgens onder het team verdeeld. Hierna heb ik in de groepsapp een berichtje gestuurd over de status van het project, wat ik in Trello heb gedaan en wat ik van iedereen verwacht deze week.

Verder heb ik twee kennissen bevraagd doormiddel van een laddering interview, dit waren interviews van ongeveer 30min met een alleenstaande moeder en een oudere vrouw. Het verloop van het interview en de gevonden kernwaarden heb ik vervolgens uitgewerkt in een stroomdiagram.
