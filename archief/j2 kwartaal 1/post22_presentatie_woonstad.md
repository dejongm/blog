---
title: Presentatie Woonstad
date: 2018-10-19
---

In de ochtend heb ik een scenario uitgeschreven van hoe een oudere man thuis een reparatiemelding kan aanmaken via het control panel. Dit willen we gebruiken tijdens de presentatie in de middag. Verder heb ik met mijn team doorgenomen wie wat gaat presenteren. Ik doe de introductie en leg uit wat ons onderzoek ons heeft opgeleverd. Mijn team presenteert de prototypes die zij hebben uitgewerkt.

Voor de mevrouw die langs kwam was Leefbaar Rotterdam het meest interessante concept. We gaan onderzoeken of dit concept mogelijk inzetbaar is. Verder hebben we veel goede feedback verkregen waar we zeker wat mee kunnen. Verder verliep de presenatie soepel en hebben we onze concepten goed beargumenteerd. Ook zijn de vragen die we kregen goed beantwoord door ons.

Feedback die wij hebben verkregen is:

-	Leefstad Rotterdam erg interessant
-	Hoe haalbaar maken, kijk naar andere community ’s hoe die het hebben aangepakt en die de grond hebben gelegd om met verschillende takken in verbinding te komen.
-	Wellicht huurkorting wanneer je je inzet in de community.
  Workshops geven
	Elementen organiseren
-	Geen geld verwachten van huurders, vooral bij onze doelgroep (Sociale huurders) is er weinig tot geen geld te besteden aan extra elementen. Bedenk daarom op welke manier dit gefinancierd kan worden.
-	Hoe ga je het een community maken, er is niet in 1 keer “leefstad Rotterdam”. Dit heeft eigenlijk veel tijd nodig, hoe zouden we dit zo snel mogelijk kunnen opmaken.
-	Doelgroep breder maken. Niet alleen de 50+ers in kaart brengen, maar juist de oudere doelgroep laten mengen met een jongere doelgroep. Zodat ieder wijs kan worden van elkaar en dat ieder binnen de community elkaar kan helpen op een gratis of voordelige manier.
-	Wij als team moeten wel letten op de kosten bij de implementatie. Daarnaast waren verder alle kritiekpunten helder.

Na de presentatie zijn we de rest van de avond bezig geweest om ons ontwerpverslag verder in InDesign uit te werken.
