---
title: Uitwerken Verslag
date: 2018-09-21
---

In de middag heb ik met m’n team gesproken over onze voortgang en waar we nu mee aan de slag kunnen. We lopen nu redelijk op schema en iedereen heeft iets waar hij of zij mee aan de slag kan. Vandaag ben ik begonnen met het uitwerken van het bezoek aan de huismeester. Dit wil ik zo uitgebreid mogelijk beschrijven en hier foto’s aan toevoegen. Vervolgens ga ik hier conclusies uit halen die ik kan gebruiken voor de ideevorming.

Ik heb vandaag ook kennissen benaderd die tot de doelgroep behoren. Ik wil hun interviewen door middel van een laddering interview.
